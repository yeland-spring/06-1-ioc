package com.twuc.webApp.yourTurn;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SimpleDependentFactory {
    @Bean
    SimpleDependent createSimpleDependent() {
        SimpleDependent simpleDependent = new SimpleDependent();
        simpleDependent.setName("O_o");
        return simpleDependent;
    }
}
