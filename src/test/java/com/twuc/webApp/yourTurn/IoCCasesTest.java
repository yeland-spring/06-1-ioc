package com.twuc.webApp.yourTurn;

import com.twuc.webApp.OutOfScanningScope;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class IoCCasesTest {
    @Test
    void should_create_object_using_annotated_application_context() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");
        WithoutDependency withoutDependency = context.getBean(WithoutDependency.class);
        assertNotNull(withoutDependency);
        assertSame(WithoutDependency.class, withoutDependency.getClass());
    }

    @Test
    void should_create_object_with_dependency() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");
        WithDependency withDependency = context.getBean(WithDependency.class);
        assertNotNull(withDependency);
        assertSame(WithDependency.class, withDependency.getClass());
        assertNotNull(withDependency.getDependency());
        assertSame(Dependent.class, withDependency.getDependency().getClass());
    }

    @Test
    void should_not_create_object_when_out_of_scanning_scope() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp.yourTurn");
        OutOfScanningScope outOfScanningScope = context.getBean(OutOfScanningScope.class);
        assertNull(outOfScanningScope);
    }

    @Test
    void should_create_object_using_interface() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");
        Interface interfaceImpl = context.getBean(Interface.class);
        assertNotNull(interfaceImpl);
        assertSame(InterfaceImpl.class, interfaceImpl.getClass());
    }

    @Test
    void should_return_name_using_configuration() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");
        SimpleObject simpleObject = (SimpleObject) context.getBean(SimpleInterface.class);
        assertNotNull(simpleObject);
        assertSame(SimpleObject.class, simpleObject.getClass());
        assertSame(SimpleDependent.class, simpleObject.getSimpleDependent().getClass());
        assertEquals("O_o", simpleObject.getSimpleDependent().getName());
    }

    @Test
    void should_choose_constructor_when_multiple_constructor() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");
        MultipleConstructor multipleConstructor = context.getBean(MultipleConstructor.class);
        assertNotNull(multipleConstructor);
        assertSame(MultipleConstructor.class, multipleConstructor.getClass());
        assertNotNull(multipleConstructor.getDependent());
        assertSame(Dependent.class, multipleConstructor.getDependent().getClass());
    }

    @Test
    void should_transfer_constructor_and_function() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");
        WithAutowiredMethod withAutowiredMethod = context.getBean(WithAutowiredMethod.class);
        assertNotNull(withAutowiredMethod.getDependent());
        assertNotNull(withAutowiredMethod.getAnotherDependent());
        assertEquals("dependent", withAutowiredMethod.getList().get(0));
        assertEquals("anotherDependent", withAutowiredMethod.getList().get(1));
    }

    @Test
    void should_create_multiple_implements() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");
        Map<String, InterfaceWithMultipleImpls> implsMap = context.getBeansOfType(InterfaceWithMultipleImpls.class);
        assertSame(3, implsMap.size());
        assertSame(ImplementationA.class, implsMap.get("implementationA").getClass());
        assertSame(ImplementationB.class, implsMap.get("implementationB").getClass());
        assertSame(ImplementationC.class, implsMap.get("implementationC").getClass());
    }
}