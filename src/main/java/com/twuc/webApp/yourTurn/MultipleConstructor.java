package com.twuc.webApp.yourTurn;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MultipleConstructor {
    private Dependent dependent;
    private String name;

    @Autowired
    public MultipleConstructor(Dependent dependent) {
        this.dependent = dependent;
    }

    public MultipleConstructor(String name) {
        this.name = name;
    }

    public Dependent getDependent() {
        return dependent;
    }

    public String getName() {
        return name;
    }
}
