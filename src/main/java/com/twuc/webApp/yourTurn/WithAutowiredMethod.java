package com.twuc.webApp.yourTurn;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class WithAutowiredMethod {
    private Dependent dependent;
    private AnotherDependent anotherDependent;
    private List<String> list = new ArrayList<>();

    public WithAutowiredMethod(Dependent dependent) {
        list.add("dependent");
        this.dependent = dependent;
    }

    public Dependent getDependent() {
        return dependent;
    }

    @Autowired
    void initialize(AnotherDependent anotherDependent) {
        list.add("anotherDependent");
        this.anotherDependent = anotherDependent;
    }

    public AnotherDependent getAnotherDependent() {
        return anotherDependent;
    }

    public List<String> getList() {
        return list;
    }
}
